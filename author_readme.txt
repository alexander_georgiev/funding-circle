Modern browsers are supported aswel as IE9.
HTML:
1 - ordinary structure;
>container;
>header + header:after > h1 + button ;
>content > table >thead (visible only on desktop);
>tbody> each <td> has a div.head for visualising the header in each table cell on mobile devices;
>simple paginator with anchor tags;

CSS:
1. theme.scss is the main stylesheet
2. global styles;
3. emelents - mobile first;
4. @media queries (
		4.1. 320px
		4.2. 480px
		4.3. 768px
		4.4. 1140px
)

This is a simplified sass structure.Usualy I would make it modular and every stylesheet would be particular for its relevant view.Something like this: http://prntscr.com/5h27tz .
